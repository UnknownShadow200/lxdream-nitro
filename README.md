# lxdream-nitro

lxdream-nitro is an opinionated fork of lxdream. These are its goals:

 - To make it easy to contribute to
   - Moving to a modern build system (meson)
   - Setting up automated and nightly builds
   - Leveraging all the tools that GitLab hosting offers
 - To make lxdream more modern
   - Moving to Gtk+ 3
   - Dropping "legacy" drivers (e.g. esound)
 - Focussing on the desktop
   - Dropping Android support
   - Making GTK a hard requirement
 - Helping DC indies test their games
   - Focus on supporting their workflows
   - Fixing bugs
   - Improving accuracy


## Build Instructions
```
git clone https://gitlab.com/simulant/community/lxdream-nitro.git
cd lxdream-nitro
meson setup builddir
meson compile -C builddir
./builddir/lxdream-nitro -h
```

## Runtime Flags
Default value is first and **bolded**
### --ram-size=SIZE
Valid SIZEs: [**16**, 32]

Allows emulating the [32MB RAM Modification](https://dreamcast.wiki/32MB_RAM_expansion).
This is implemented by increasing the memory for the SH4 CPU
and changing the mask bits used for RAM reads and writes.
