project(
    'lxdream-nitro',
    'c', 'cpp',
    default_options: ['c_std=c99', 'cpp_std=c++11']
)

#
## Variables
#

conf_data = configuration_data()
c_compiler = meson.get_compiler('c')
is_linux = host_machine.system() == 'linux'
is_apple = host_machine.system() == 'darwin'
is_windows = host_machine.system() == 'windows'
install = is_linux # Not used for macOS as it has a custom install script to create the app bundle
glib_dep = dependency('glib-2.0', required: true)

is_x86 = host_machine.cpu_family() == 'x86'
is_x64 = host_machine.cpu_family() == 'x86_64'

#
## Default configuration data
#

# Package configs
conf_data.set('package', 'lxdream-nitro')
conf_data.set('package_string', 'lxdream-nitro 0.9.1')
conf_data.set('package_version', '0.9.1')
conf_data.set('package_tarname', 'lxdream-nitro')
conf_data.set('build_plugins', 1)
conf_data.set('prefix', get_option('prefix'))
conf_data.set('sed_program', find_program('sed').path())
conf_data.set('cpp_program', find_program('gcc').path() + ' -E -L' + meson.current_build_dir())
conf_data.set('is_apple', is_apple ? 1 : 0)
conf_data.set('is_windows', is_windows ? 1 : 0)
conf_data.set('is_linux', is_linux ? 1 : 0)
conf_data.set('is_x86', is_x86 ? 1 : 0)
conf_data.set('is_x64', is_x64 ? 1 : 0)

# Dependency configs
conf_data.set('has_glx', 0)
conf_data.set('has_gles2', 0)
conf_data.set('has_osmesa', 0)
conf_data.set('has_esound', 0)
conf_data.set('has_sdl', 0)
conf_data.set('has_core_audio', 0)
conf_data.set('has_alsa', 0)
conf_data.set('has_pulse', 0)
conf_data.set('has_gtk', 0)

#
## C compiler configuration
#

add_global_arguments(
    '-D_GNU_SOURCE',
    '-mfpmath=sse',
    '-msse2',
    '-fexceptions',
    '-D_POSIX_C_SOURCE=200809L',
    '-fno-strict-aliasing',
    '-fomit-frame-pointer',
    '-O2',
    '-Wall',
    language: 'c'
)

conf_data.set('sizeof_voidp', c_compiler.sizeof('void*'))
conf_data.set('has_sys_stat_h', c_compiler.has_header('sys/stat.h') ? 1 : 0)
conf_data.set('has_sys_types_h', c_compiler.has_header('sys/types.h') ? 1 : 0)
conf_data.set('has_unistd_h', c_compiler.has_header('unistd.h') ? 1 : 0)

#
## Common sources
#

lxdream_sources = files(
    'src/asic.c',
    'src/bios.c',
    'src/bootstrap.c',
    'src/config.c',
    'src/cpu.c',
    'src/dcload.c',
    'src/display.c',
    'src/dreamcast.c',
    'src/eventq.c',
    'src/gdbserver.c',
    'src/gdlist.c',
    'src/version.c',
    #'src/gui_none.c',
    'src/hotkeys.c',
    'src/ioutil.c',
    'src/loader.c',
    'src/lxpaths.c',
    'src/main.c',
    'src/mem.c',
    'src/plugin.c',
    'src/sdram.c',
    'src/symtable.c',
    'src/syscall.c',
    'src/tqueue.c',
    'src/util.c',
    'src/watch.c',
    'src/xlat/xltcache.c',
    'src/xlat/xlatdasm.c',
    'src/xlat/disasm/dis-init.c',
    'src/xlat/disasm/dis-buf.c',
    'src/xlat/disasm/i386-dis.c',
    'src/aica/audio.c',
    'src/sh4/sh4.c',
    'src/sh4/mmu.c',
    'src/sh4/mmux86.c',
    'src/sh4/cache.c',
    'src/sh4/timer.c',
    'src/sh4/dmac.c',
    'src/sh4/pmm.c',
    'src/sh4/sh4mem.c',
    'src/sh4/sh4mmio.c',
    'src/sh4/sh4trans.c',
    'src/sh4/shadow.c',
    'src/sh4/intc.c',
    'src/sh4/scif.c',
    'src/aica/aica.c',
    'src/aica/armcore.c',
    'src/aica/armmem.c',
    'src/aica/armdasm.c',
    'src/aica/audio.c',
    'src/pvr2/pvr2mem.c',
    'src/pvr2/tacore.c',
    'src/pvr2/pvr2.c',
    'src/pvr2/scene.c',
    'src/pvr2/glutil.c',
    'src/pvr2/glrender.c',
    'src/pvr2/texcache.c',
    'src/pvr2/yuv.c',
    'src/pvr2/rendsave.c',
    'src/pvr2/rendsort.c',
    'src/pvr2/scene.c',
    'src/display.c',
    'src/gdrom/ide.c',
    'src/gdrom/gdrom.c',
    'src/drivers/cdrom/cdrom.c',
    'src/drivers/cdrom/sector.c',
    'src/drivers/cdrom/drive.c',
    'src/drivers/cdrom/cd_gdi.c',
    'src/drivers/cdrom/isofs.c',
    'src/drivers/cdrom/isomem.c',
    'src/drivers/cdrom/edc_ecc.c',
    'src/drivers/cdrom/cd_cdi.c',
    'src/drivers/cdrom/cd_nrg.c',
    'src/drivers/cdrom/cd_mmc.c',
    'src/drivers/audio_null.c',
    'src/drivers/video_null.c',
    'src/drivers/gl_fbo.c',
    'src/drivers/gl_sl.c',
    'src/drivers/serial_unix.c',
    'src/drivers/video_gl.c',
    'src/drivers/render_gl.c',
    'src/maple/maple.c',
    'src/maple/kbd.c',
    'src/maple/lightgun.c',
    'src/maple/mouse.c',
    'src/maple/controller.c',
    'src/maple/vmu.c',
    'src/vmu/vmulist.c',
    'src/vmu/vmuvol.c',
    'src/gtkui/gtkcb.c',
    'src/gtkui/gtk_cfg.c',
    'src/gtkui/gtk_ctrl.c',
    'src/gtkui/gtk_debug.c',
    'src/gtkui/gtk_dump.c',
    'src/gtkui/gtk_gd.c',
    'src/gtkui/gtk_mmio.c',
    'src/gtkui/gtkui.c',
    'src/gtkui/gtk_win.c',
)

#
## Common dependencies
#
if host_machine.system() == 'windows' or host_machine.system().endswith('bsd') or \
   host_machine.system() == 'dragonfly'
    libdl = declare_dependency()
else
    libdl = c_compiler.find_library('dl', required : true)
endif

lxdream_deps = [
    glib_dep,
    dependency('gtk+-3.0', required: true),
    dependency('libisofs-1', required: true),
    dependency('zlib', required: true),
    dependency('libpng', required: true),
    dependency('gl', required: true),
    c_compiler.find_library('m', required: true),
    libdl
]

include_dirs = ['src']

#
## Linux-specific additions
#

if is_linux
    lxdream_sources += [
        'src/paths_unix.c',
        'src/drivers/cdrom/cd_linux.c',
        'src/drivers/io_glib.c',
        'src/drivers/video_gtk.c',
        'src/drivers/joy_linux.c',
    ]

    include_dirs = include_directories('src', '.')

    alsa_dep = dependency('alsa', required: false)
    if alsa_dep.found()
        lxdream_sources += ['src/drivers/audio_alsa.c']
        conf_data.set('has_alsa', 1)
    endif

    pulse_dep = dependency('libpulse-simple', required: false)
    if pulse_dep.found()
        lxdream_sources += ['src/drivers/audio_pulse.c']
        conf_data.set('has_pulse', 1)
    endif

    lxdream_deps += [
        alsa_dep,
        pulse_dep,
    ]

    conf_data.set('has_gtk', 1)
endif

#
## Apple-specific additions
#

if is_apple
    # Allow compiling .m files
    add_languages('objc')

    # Enable ARC
    add_global_arguments(
        '-fobjc-arc',
        language: 'objc'
    )

    # Silence OpenGL deprecation warnings
    add_global_arguments(
        '-DGL_SILENCE_DEPRECATION',
        language: ['c', 'objc']
    )

    # Fix Dl_info undeclared
    add_global_arguments(
        '-D_DARWIN_C_SOURCE=200809L',
        language: 'c'
    )

    lxdream_sources += [
        'src/drivers/audio_osx.m',
        'src/drivers/cdrom/cd_osx.c',
        'src/drivers/io_osx.m',
        'src/drivers/osx_iokit.m',
    ]

     # Include directories for glib
    include_dirs = include_directories('src', '.', '/usr/local/include/glib-2.0', '/usr/local/lib/glib-2.0/include')

    lxdream_deps += [
        dependency('appleframeworks', modules : 'Foundation', required : true),
        dependency('appleframeworks', modules : 'Cocoa', required : true),
        dependency('appleframeworks', modules : 'Carbon', required : true),
        dependency('appleframeworks', modules : 'AppKit', required : true),
        dependency('appleframeworks', modules : 'IOKit', required : true),
        dependency('appleframeworks', modules : 'CoreAudio', required : true),
        dependency('appleframeworks', modules : 'OpenGL', required : true)
    ]

    conf_data.set('has_core_audio', 1)

    # Add version number to Info.plist
    configure_file(input : 'Info.plist.in',
                   output : 'Info.plist',
                   configuration : conf_data)

    # Add custom install script to build app bundle (run using `meson install`)
    # TODO: Update version number in Info.plist
    meson.add_install_script('build_app_bundle.sh')
endif

#
## Generate configuration
#

configure_file(
    input: 'config.h.in',
    output: 'projconfig.h',
    configuration: conf_data
)

#
## GLSL shader header
#

genglsl = executable(
    'genglsl',
    files('src/tools/genglsl.c', 'src/tools/insparse.c', 'src/tools/actparse.c'),
    dependencies: glib_dep,
    include_directories: include_dirs,
    c_args: ['-O0'],
)

shaders_h = custom_target(
    'shaders.h',
    output: 'shaders.h',
    input: 'src/pvr2/shaders.glsl',
    command: [genglsl, '@INPUT@']
)

lxdream_sources += [
    shaders_h
]

#
## SH4 decoder
#

gendec = executable(
    'gendec',
    files('src/tools/gendec.c', 'src/tools/insparse.c', 'src/tools/actparse.c'),
    dependencies: glib_dep,
    include_directories: include_dirs,
)

gendec_path = gendec.full_path()

gen = generator(
    gendec,
    arguments: ['@CURRENT_SOURCE_DIR@/src/sh4/sh4.def', '@INPUT@', '--output=@OUTPUT@'], output: '@BASENAME@.c',
)


lxdream_sources += gen.process(
    'src/sh4/sh4core.in',
    'src/sh4/sh4dasm.in',
    'src/sh4/sh4x86.in',
    'src/sh4/sh4stat.in',
    preserve_path_from: meson.current_source_dir()
)

#
## Final Executables
#

executable(
    'genmach',
    files(
        'src/tools/genmach.c',
        'src/tools/insparse.c',
        'src/tools/actparse.c',
        'src/tools/mdparse.c'
    ),
    dependencies: glib_dep,
    include_directories: include_dirs
)

executable(
    'lxdream-nitro',
    lxdream_sources,
    dependencies: lxdream_deps,
    include_directories: include_dirs,
    install: install
)

# Linux specific installation instructions
if is_linux
    install_data([
            'pixmaps/lxdream.png',
            'pixmaps/tb-cdrom.png',
            'pixmaps/tb-load.png',
            'pixmaps/tb-pause.png',
            'pixmaps/tb-reset.png',
            'pixmaps/tb-run-to.png',
            'pixmaps/tb-single-step.png',
            'pixmaps/tb-breakpoint.png',
            'pixmaps/tb-ctrls.png',
            'pixmaps/tb-paths.png',
            'pixmaps/tb-preferences.png',
            'pixmaps/tb-run.png',
            'pixmaps/tb-save.png',
        ], install_dir: 'share/pixmaps/lxdream-nitro'
    )
    install_data(['pixmaps/lxdream.png'], install_dir:'share/pixmaps', rename: ['lxdream-nitro.png'])
endif
