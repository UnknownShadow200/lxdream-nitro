/**
 * $Id$
 * 
 * Interface declarations for the symbol table routines (symtable.c)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef lxdream_symtable_H
#define lxdream_symtable_H 1

#include <stdint.h>

struct sym_symbol {
	const char *name;
	uint32_t address; /* sh4addr_t */
	unsigned size;
};

struct sym_table;
typedef void (*sym_table_destroy_cb)(struct sym_symbol *entries, unsigned size);

struct sym_table {
	struct sym_symbol* entries;
	unsigned num_entries;
	sym_table_destroy_cb destroy_entries;
};


const char *sym_table_get_symbol( struct sym_table *table, uint32_t addr );

/**
 * Set the entries of the symbol lookup table. The table will be modified
 * to sort it by address and eliminate duplicates.
 * The callback supplied is invoked whenever the table is changed
 * or removed.
 */
void sym_table_set_entries( struct sym_table *table, struct sym_symbol *entries, unsigned num_entries, sym_table_destroy_cb callback );

#endif /* !lxdream_symtable_H */
