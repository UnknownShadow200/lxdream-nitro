/**
 * $Id$
 * 
 * Manages a lookup table of symbols
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include "symtable.h"
#include <string.h>

const char *sym_table_get_symbol( struct sym_table *table, uint32_t addr )
{
	int l = 0, h = table->num_entries;
	while( l != h ) {
	    int i = l + (h-l)/2;
	    int iaddr = table->entries[i].address;
	    if( iaddr == addr ) {
	        return table->entries[i].name;
	    } else if( iaddr > addr ) {
	        h = i;
	    } else { /* iaddr < addr */
	        l = i+1;
	    }
	}
	return NULL;
}

static void swap_symbol( struct sym_symbol *a, struct sym_symbol *b ) 
{
    struct sym_symbol tmp;
    if( a == b )
        return;
    memcpy( &tmp, a, sizeof( struct sym_symbol ) );
    memcpy( a,    b, sizeof( struct sym_symbol ) );
    memcpy( b, &tmp, sizeof( struct sym_symbol ) );
}

static unsigned sort_symtab( struct sym_symbol *table, unsigned numEntries ) 
{
    /* Implement via simple selection sort for now; usually we don't have very
     * large symbol tables.
     */
    for( unsigned i = 0; i < numEntries; i++ ) {
        struct sym_symbol *next_entry = &table[i];
        for( unsigned j = i + 1; j < numEntries; ) {
            if( table[j].address < next_entry->address ) {
                next_entry = &table[j];
                j++;
            } else if( table[j].address == next_entry->address ) {
                /* Duplicate - kill it */
                swap_symbol( &table[j], &table[--numEntries] );
            } else {
                j++;
            }
        }
        swap_symbol( &table[i], next_entry );
    }
    return numEntries;
}

void sym_table_set_entries( struct sym_table *table, struct sym_symbol *entries, unsigned num_entries, sym_table_destroy_cb callback )
{
    if( table->destroy_entries != NULL ) {
        table->destroy_entries( table->entries, table->num_entries );
    }
    table->entries = entries;
    table->destroy_entries = callback;

    if( entries == NULL ) {
        table->num_entries = 0;
    } else {
        table->num_entries = sort_symtab( entries, num_entries );
    }
}
